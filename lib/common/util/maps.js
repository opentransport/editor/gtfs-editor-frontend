// @flow

export function defaultTileURL (mapId: ?string): string {
  // const MAP_MAP_ID = mapId || process.env.MAP_MAP_ID
  // if (!MAP_MAP_ID ) {
  //   throw new Error('Mapbox ID and token not defined')
  // }
  return `https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png`;
}
