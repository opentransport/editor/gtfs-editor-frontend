#!/bin/sh
set -e

# Build image
echo Building GTFS EDITOR Frontend
docker build --tag="opentransport/gtfs-editor-frontend:builder" -f Dockerfile.builder .

docker run --rm --entrypoint tar "opentransport/gtfs-editor-frontend:builder" -c dist|tar x -C ./
#package GTFS EDITOR quietly while keeping travis happpy
docker build --tag="opentransport/gtfs-editor-frontend" -f Dockerfile .