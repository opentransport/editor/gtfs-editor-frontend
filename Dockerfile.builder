FROM node:12-alpine

RUN apk add --update yarn git

ENV EDITOR_BACKEND_ROOT="/opt/gtfs-editor"

WORKDIR ${EDITOR_BACKEND_ROOT}

ADD package.json ${EDITOR_BACKEND_ROOT}/package.json
ADD gtfs.yml ${EDITOR_BACKEND_ROOT}/gtfs.yml
ADD gtfsplus.yml ${EDITOR_BACKEND_ROOT}/gtfsplus.yml
ADD yarn.lock ${EDITOR_BACKEND_ROOT}/yarn.lock
ADD lib ${EDITOR_BACKEND_ROOT}/lib
ADD i18n ${EDITOR_BACKEND_ROOT}/i18n
ADD configurations ${EDITOR_BACKEND_ROOT}/configurations
ADD scripts ${EDITOR_BACKEND_ROOT}/scripts
ADD .git ${EDITOR_BACKEND_ROOT}/.git

# Build OTP
RUN yarn && yarn build
